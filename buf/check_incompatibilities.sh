#!/usr/bin/env bash

set -eExuo pipefail

if [[ -n "$(git rev-parse -q --verify MERGE_HEAD)" ]]; then
    echo "Currently merging, skipping buf checks"
    exit 0
fi

SYSTEM=$(uname|tr '[:upper:]' '[:lower:]')
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

if [[ "${CI:-}" == "true" ]]; then
    echo "Fetch the master branch"
    git fetch origin master:master
fi

cd "$(git rev-parse --show-toplevel)"
MERGE_BASE="$(git merge-base HEAD master)"
"$SCRIPT_DIR"/"$SYSTEM"/buf breaking --against ".git#ref=$MERGE_BASE" "$@"
