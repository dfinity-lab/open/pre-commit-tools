# pre-commit tools

A suite of tools for our pre-commit use.


## Details

This repository contains vendored executables of the utilities that we use in the DFINITY repo.
Files for the utilities are stored inside the repo directoy.

To upgrade the versions of these utilities, just download the new version, replace the existing
one with it, and change the git commit hash used in any dependent pre-commit files.

* nixpkgs-fmt: https://github.com/nix-community/nixpkgs-fmt
* buf: https://docs.buf.build/installation
* rustfmt: https://github.com/rust-lang/rustfmt/releases
* shfmt: https://github.com/mvdan/sh
* ormolu: https://github.com/tweag/ormolu/releases
