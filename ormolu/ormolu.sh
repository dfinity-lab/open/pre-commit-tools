#!/usr/bin/env bash
set -euo pipefail

SYSTEM=$(uname|tr '[:upper:]' '[:lower:]')
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
"$SCRIPT_DIR"/"$SYSTEM"/ormolu --mode inplace "$@"
